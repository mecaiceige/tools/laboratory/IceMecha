# Welcome to the Ice Mechanics Platform

This page is aiming to centralized all the tools/competencies available in the ice mechanic team at IGE. If you have any question or you want to initiate collaboration feel free to contact us.

Some documentations are incomplete and will be completed with time.

## Tools

::::{grid} 1 1 2 2 
:class-container: text-center 
:gutter: 3

:::{grid-item-card} 
:link: docs/Tools/AITA/aita
:link-type: doc
:class-header: bg-light

AITA
^^^
```{image} docs/Tools/AITA/aita_pf.png
:alt: aita_pf
:class: bg-primary mb-1
:width: 200px
:align: center
```

Optical orientation measurement

:::

:::{grid-item-card} 
:link: docs/Tools/DIC/dic
:link-type: doc
:class-header: bg-light

DIC
^^^

```{image} docs/Tools/DIC/logo_DIC.png
:alt: aita_pf
:class: bg-primary mb-1
:width: 200px
:align: center
```

Strain field measurement using **D**igital **I**mages **C**orrelation.
:::

:::{grid-item-card} 
:link: docs/Tools/EBSD/ebsd
:link-type: doc
:class-header: bg-light

EBSD
^^^
```{image} docs/Tools/EBSD/EBSD_logo.png
:alt: ebsd
:class: bg-primary mb-1
:width: 200px
:align: center
```

Crystallographic measurement using **E**lectron **B**ack**S**catter **D**iffraction.
:::

:::{grid-item-card} 
:link: docs/Tools/AE/ae
:link-type: doc
:class-header: bg-light

Acoustic Emission
^^^

```{image} docs/Tools/AE/logo_AE.png
:alt: aita_pf
:class: bg-primary mb-1
:width: 200px
:align: center
```



Acoustic Emission

:::

::::

## Testing machine

::::{grid} 1 1 1 3
:class-container: text-center 
:gutter: 3

:::{grid-item-card} 
:link: docs/Testing_Machine/creep_press/creep_press
:link-type: doc
:class-header: bg-light

Creep press
^^^
```{image} docs/Tools/DIC/logo_DIC.png
:alt: aita_pf
:class: bg-primary mb-1
:width: 200px
:align: center
```

Imposed stress press

:::

:::{grid-item-card} 
:link: https://mecaiceige.gricad-pages.univ-grenoble-alpes.fr/tools/laboratory/SR-Press/main.html
:class-header: bg-light

Strain rate press
^^^

```{image} docs/Tools/DIC/logo_DIC.png
:alt: aita_pf
:class: bg-primary mb-1
:width: 200px
:align: center
```

Control strain rate press
:::

:::{grid-item-card} 
:link: docs/Testing_Machine/creep_torsion/creep_torsion
:link-type: doc
:class-header: bg-light

Torsion press
^^^
```{image} docs/Tools/DIC/logo_DIC.png
:alt: ebsd
:class: bg-primary mb-1
:width: 200px
:align: center
```

Torsion imposed stress press
:::


::::

## Laboratory ice sample

::::{grid} 1 1 1 3
:class-container: text-center 
:gutter: 3

:::{grid-item-card} 
:link: docs/Ice_Sample/granular_ice
:link-type: doc
:class-header: bg-light

Granular Ice
^^^
```{image} docs/Ice_Sample/logo_GI.png
:alt: aita_pf
:class: bg-primary mb-1
:width: 200px
:align: center
```

Synthesis protocol

:::

:::{grid-item-card} 
:link: docs/Ice_Sample/columnar_ice
:link-type: doc
:class-header: bg-light

Columnar Ice
^^^

```{image} docs/Tools/AITA/logo_TS.png
:alt: aita_pf
:class: bg-primary mb-1
:width: 200px
:align: center
```

Synthesis protocol
:::

:::{grid-item-card} 
:link: docs/Ice_Sample/mono_crystal
:link-type: doc
:class-header: bg-light
Mono crystal
^^^

```{image} docs/Ice_Sample/logo_BI.png
:alt: aita_pf
:class: bg-primary mb-1
:width: 200px
:align: center
```

Synthesis protocol
:::


::::

