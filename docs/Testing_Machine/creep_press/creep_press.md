# Creep imposed stress press

Here at IGE we have uniaxial creep creep apparatus for performing **unconfined** creep tests.

```{figure} creep_press.svg
---
height: 500px
name: peltier
---
Creep apparatus equipped with a camera setup for Digital Image Correlation (DIC).
```

## Sample

:::{tip}
**Teflon plates** are used at the interface between the apparatus and the ice to minimize friction and prevent shearing.
:::

:::{tip}
The ice sample is protected by a **plastic cover** to prevent sublimation caused by air flow around the sample. The type of cover used depends on whether DIC measurements are being performed during the test.
:::

## Applied Stress

The applied stress is vertical, and the force is related to the applied mass (bottom left picture) via a pulley system. The relationship between the applied mass and the force depends on the configuration of the system, including the position of the mass and counter mass. As of March 20, 2023, the current configuration has the following relationship:

$$Force = 0.14*Mass+22.38$$

with:

- $Mass\sim g$
- $Force\sim kg.m.s^{-2} (N)$

:::{tip}
In the case of a truncated cylinder, you can use this app to quickly compute the surface area and mass using the formula mentioned above. [(link to app)](https://ige-creep.streamlit.app/).

The demonstration of the surface area of a truncated cylinder is availaible here [{bdg-primary}`Demo`](Demonstration_Area_Calcul).
:::

## DIC
For a description and tips on DIC, please refer to the following link: [DIC protocol](../../Tools/DIC/dic_protocol.md). 
