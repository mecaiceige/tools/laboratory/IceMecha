(content:DIC)=
# DIC

::::{card-carousel} 3


:::{card}
:margin: 3
:class-body: text-center
:class-header: bg-light text-center
:link: dic_protocol
:link-type: doc

**Protocol**
^^^
```{image} logo_DPI.png
:alt: aita_pf
:class: bg-primary mb-1
:width: 200px
:align: center
```

DIC protocol : speckle preparation and camera set-up
:::


:::{card}
:margin: 3
:class-body: text-center
:class-header: bg-light text-center
:link: dic_spam
:link-type: doc

**DIC software**
^^^

```{image} logo_spam.jpeg
:alt: spam
:class: bg-primary mb-1
:width: 200px
:align: center
```


Spam
:::



:::{card}
:margin: 3
:class-body: text-center
:class-header: bg-light text-center
:link: https://mecaiceige.gricad-pages.univ-grenoble-alpes.fr/tools/documentations/DIC-Book/docs/intro.html

**DIC post processing**
^^^

```{image} logo_DIC_TB.png
:alt: aita_pf
:class: bg-primary mb-1
:width: 200px
:align: center
```


A guidebook for data analysis of strain field
:::