# DIC Protocol

Firstly, it is important to ensure a flat surface as we are only performing 2D digital image correlation. Therefore, for columnar ice, we work with cubic-shaped samples, and for granular ice, we work with truncated cylinder-shaped samples.


## Prepare the sample surface
For accurate DIC measurements, we need to mark the surface of the ice sample, as there is no natural contrast. These surface markings are called **speckles**.

To create speckles, we use shoe polish to fill in the holes made by sandpaper on the ice surface. Here is a detailed procedure:

1. Use sandpaper to scratch the surface of the ice sample with small rotations in order to create "random" holes.
2. Apply black shoe polish to the surface with a toothbrush or a paper tissue.

A good speckle has a small correlation radius (approximately 6 pixels) and is dark (with a gray level distribution skewed to the left). Below are some examples of correctly created speckles that can be used for DIC on ice.

```{figure} speckles.svg
---
height: 500px
name: speckles
---
Two different speckles that have been used on ice to achieve accurate DIC measurements. **(a)** Speckles created by applying shoe polish to the sample surface. **(b)** Gray level distribution of the speckle. **(c)** Correlation length as a function of the orientation.
```

:::{warning}
The type of speckle used for DIC measurements on ice has some limitations, particularly in terms of its lifetime. Due to sublimation, the speckle can degrade over time, which is why it is important to protect the sample from air flow using some kind of covering (as shown in the picture below). Based on our experience, the speckle typically lasts for 48 to 72 hours. For longer experiments, it may be necessary to redo the speckle on the aperture without unloading the sample. However, in our experience, the speckle properties such as correlation length and gray level distribution tend to be worse when this is done, which can affect the accuracy of the DIC measurements.
:::



## Prepare the DIC sep-up

We use a camera and lighting setup provided by the [CMTC-Grenoble INP service](https://cmtc.grenoble-inp.fr/). Various configurations are possible, but we currently use a Nikon D850 camera with a 180 mm focal length and two lights from the CMTC. In addition, we use two LED panels (not visible) positioned on the aperture for additional lighting.


```{figure} ../../Testing_Machine/creep_press/creep_press.svg
---
height: 500px
name: aperture
---
Creep apparatus equipped with a camera setup for Digital Image Correlation (DIC).
```

1. Set up the camera and lights, ensuring that the optic axis of the camera is as perpendicular as possible to the sample surface. Cover as much of the camera's sensor as possible to utilize its maximum capabilities.

    :::{tip}
    To ensure that the optic axis is perpendicular to the sample surface, take you time and my tips are :

    1. Check that the left and right edges of the sample have the same length in the picture.
    2. With a wide aperture make sure that the pictures is focus everywhere.
    :::

2. Use manual mode to fix the camera settings, and keep them constant throughout the experiment. It's better to under-expose the pictures than to over-expose them, as this will provide better results for the DIC.

    :::{tip}
    For the Nikon D850 with an 180mm focal length, use the following camera settings:
    
    1. focal aperture (f/10)
    2. ISO value (320)
    3. Exposure time to achieve good gray level distribution
    :::
3. Configure other camera settings, such as adding a delay between the mirror flip and the acquisition (e.g., 3 seconds on the D850) if the acquisition frequency is low.
4. Use an intervalometer to set the picture frequency. For example, take two pictures every 10 minutes.


## Tips for columnar ice

:::{note}
If you are performing DIC on columnar ice, once the camera is positioned correctly, it can be helpful to capture images using polarized filters. This will allow you to identify the grain boundaries in the DIC field. It's recommended to capture multiple images with different orientations of the polarized filters, as this will make it easier to position the grain boundaries accurately.
:::