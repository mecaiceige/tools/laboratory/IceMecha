# Digital Images Correlation using spam

In order to performed Digital Images Correlation to measured displacement field you can select you favorite DIC software. ([spam](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/intro.html),[DICe](https://github.com/dicengine/dice)). At IGE, we use [spam](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/intro.html) and in order to have an efficient workflow, spam's function have been encapsulated in [spam-ice](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/lib_python/spam-ice).

## Images preprocessing

First it is needed to convert raw file (`.NEF`, `.CR2`) in `tiff` format. [`dcraw`](https://www.dechifro.org/dcraw/) is used in this purpose.

Run on all raw file you want to convert :

```shell
dcraw -a -T -d *
```

## First image preprocessing

In our experimental protocol, we use the first image for DIC that is taken before applying force on the sample. However, due to the imperfect parallelism of the two faces, a rigid motion displacement is observed between the first and second image. To improve the accuracy of DIC measurements, we recommend correcting the rigid motion displacement in the first image before performing full DIC analysis using the following method:

```shell
spam-reg im1.tiff im2.tiff -it 200 -def -rig -bb 128 -be 2 -mf1 mf1.tiff
```

`mf1.tiff ` is a mask of `im1.tiff` where only the speckle area is selected (white = selected, black = none selected)

This command is creating a `im1-reg-def.tiff` where the rigid motion found between the image 1 and 2 is apply on image 1. Replace `im1.tiff` by this file for the DIC.

## Global DIC between image $1$ and $n$

In order to performed global DIC on a set of image we use [`spam_1_analysis`]() function from [spam-ice](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/lib_python/spam-ice). This function is wrapping the set of command from spam (`spam-pixelSearch`, `spam-ldic`, `spam-filterPhiField`, `spam-regularStrain`)

````{card}

script_1n_analysis.py
^^^
```python
import spam_ice.spam_ice as spi
import os

# DIC windows size see spam doc
hws=75

main_folder='1n_analysis/'

os.mkdir(main_folder)
os.mkdir(main_folder+'hws_'+str(hws)+'/')

# folder containing all the image that you want to correlate. (and only those images)
adr_SP1='tiff/SP1/'
# mask created on the first image of the folder (white: speckle - surface correlation, black: surface none correlated).
m1='mask/mf1.tif'
# folder output
output=main_folder+'hws_'+str(hws)+'/'+'SP1/'

# search range : correspond to the maximum displacement between 2 consecutive images in pixel. This value restrains the area of correlation for the DIC. 
box=10

spi.spam_1n_analysis(adr_SP1,m1,hws,output, box=box,filter=True,interpolate=True)
```
+++
run: python script_1n_analysis.py
````


## Incremental DIC between image $n$ and $m$