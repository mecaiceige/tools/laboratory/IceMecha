(content:aita)=
# AITA

::::{card-carousel} 3


:::{card}
:margin: 3
:class-body: text-center
:class-header: bg-light text-center
:link: AITA_preparation
:link-type: doc

**Preparation**
^^^
```{image} logo_TS.png
:alt: aita_pf
:class: bg-primary mb-1
:width: 200px
:align: center
```

Thin section preparation
:::


:::{card}
:margin: 3
:class-body: text-center
:class-header: bg-light text-center
:link: AITA_analysis
:link-type: doc
**Acquisition**
^^^

```{image} logo_G50.png
:alt: aita_pf
:class: bg-primary mb-1
:width: 200px
:align: center
```


AITA acquisition tools
:::



:::{card}
:margin: 3
:class-body: text-center
:class-header: bg-light text-center
:link: https://mecaiceige.gricad-pages.univ-grenoble-alpes.fr/tools/documentations/AITA-book/docs/intro.html

**Data Analysis**
^^^

```{image} aita_pf.png
:alt: aita_pf
:class: bg-primary mb-1
:width: 200px
:align: center
```


A guidebook for data analysis of AITA measurement
:::