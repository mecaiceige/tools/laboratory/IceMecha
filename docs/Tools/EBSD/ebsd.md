(content:EBSD)=
# EBSD

::::{card-carousel} 2


:::{card}
:margin: 3
:class-body: text-center
:class-header: bg-light text-center
:link: https://mea.edu.umontpellier.fr/ebsd/camscan-crystalprobe-x500fe/

**MEB Aperture**
^^^
```{image} EBSD_logo.png
:alt: MEB_GM
:class: bg-primary mb-1
:width: 200px
:align: center
```

MEB with cryo-stage
:::


:::{card}
:margin: 3
:class-body: text-center
:class-header: bg-light text-center
:link: https://mtex-toolbox.github.io/
**Data Analysis MTEX**
^^^

```{image} logo_MTEX.png
:alt: mtex
:class: bg-primary mb-1
:width: 200px
:align: center
```


MTEX
:::

::::